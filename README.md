此分支已经废弃，不再使用。

请访问最新的 <a href="https://gitee.com/atzlinux/debian-cn/tree/apt-install/">apt-install</a> 分支。

本项目网站首页
<https://www.atzlinux.com>

<!DOCTYPE html>
<html lang="zh_CN">
<head>
<meta http-equiv="content-type" content="text/html;charset=utf-8">
<base href="https://www.atzlinux.com/
    <!-- 包含头部信息适应不同设备 -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- 包含bootstrap 样式表-->
  <link rel="stylesheet" href="./bootstrap-4.3.1-dist/css/bootstrap.min.css">
</head>
<body>
<div class="container">
<h1 align="center">铜豌豆 Linux</h1>
<a href="https://www.atzlinux.com/index.htm">首页</a>
<a href="https://www.atzlinux.com/yjaz.htm">一键安装脚本</a>
<a href="https://www.atzlinux.com/allpackages.htm">软件包列表</a>
<a href="https://www.atzlinux.com/skills.htm">使用技巧</a>
<a href="https://www.atzlinux.com/iso-type.htm">其它版本 ISO</a>
<a href="https://www.atzlinux.com/faq.htm">常见问题</a>
<a href="https://gitee.com/atzlinux/debian-cn/issues" target="_blank">反馈问题 bug</a>
<a href="https://www.atzlinux.com/devel.htm">参与开发</a>
<a href="https://gitee.com/atzlinux/projects" target="_blank">源代码</a>
<a href="https://www.atzlinux.com/juanzeng.htm">捐赠</a>
<hr>
<h2>基于 Debian 的 Linux 中文操作系统</h2>
<p>
<a href="https://www.debian.org" target="_blank">
Debian</a> 是一款非常优秀的 Linux 操作系统，但默认安装缺少中国人常用的软件。
</p>
<p>
《<b>铜豌豆 Linux</b>》操作系统在 Debian 基础上，收集制作这些常用软件，一次性安装完成，节省大家定制 Debian 的时间,做到“<b>开箱即用</b>”。
</p>
<p>
最新版本：
2020-04-14
日发布《铜豌豆 Linux》
<a href="https://www.atzlinux.com/News/2020/20200414.htm" target="_blank">
10.3.3
版</a>。
</p>
目前收录的主要中文软件包如下：
<ul>
	<li>QQ</li>
	<li>微信</li>
	<li>百度网盘</li>
	<li>搜狗输入法</li>
	<li>网易云音乐</li>
	<li>有道词典</li>
	<li>WPS</li>
	<li>中文字体</li>
	<li>星际译王</li>
<a href="https://www.atzlinux.com/allpackages.htm" target="_blank">
中文软件完整列表</a>
</ul>
Debian 自身软件包资源也很丰富，约有 59000 个软件包，《铜豌豆 Linux》100% 兼容这些软件包，并内置 Debian 国内官方镜像源。
<ul>
<li><a href="https://www.debian.org/distrib/packages#search_packages" target="_blank">
搜索 Debian 软件包</a></li>
<li>
<a href="https://packages.debian.org/stable/" target="_blank">
Debian 软件包分类展示
</a></li>
</ul>
<h3>桌面截屏</h3>
安装完成后，各个软件在同一个屏幕展示的截屏如下：
<p>
<img  class="img-fluid" src="https://cdn.atzlinux.com/debian/img/jieping.png" alt="屏幕截屏">
</p>
<h3><a href="https://www.atzlinux.com/index.htm#demo">体验机器</a></h3>
<h3>下载</h3>
2020-04-14
日发布《铜豌豆 Linux》
10.3.3
版。
<a href="https://www.atzlinux.com/News/2020/20200414.htm"  target="_blank">
发布说明
</a>
<p>
安装文件大小约 2.6 G，下载地址如下：
</p>
<ul>
	<li>CDN 下载</li>
<p>
<a href="https://cdn.atzlinux.com/atzlinux-cd/10.3.3/amd64/iso-dvd/atzlinux-10.3.3-amd64-DVD-1.iso" target="_blank">
https://cdn.atzlinux.com/atzlinux-cd/10.3.3/amd64/iso-dvd/atzlinux-10.3.3-amd64-DVD-1.iso</a>
</p>
	<!--
	<li>百度网盘下载</li>
		<a href="https://pan.baidu.com/s/16qqu3EMr-4XrHjjKjkJbhg"  target="_blank">
	https://pan.baidu.com/s/16qqu3EMr-4XrHjjKjkJbhg
	</a>密码: 3r0h<br>
	百度网盘会员下载速度比较快，非会员用户，建议使用上面的 CDN 方式下载。<br>
	iso 文件目录路径为：
	atzlinux-cd/10.3.3/amd64/iso-dvd/atzlinux-10.3.3-amd64-DVD-1.iso
	-->
</ul>
下载的安装文件名为：<i>atzlinux-10.3.3-amd64-DVD-1.iso</i>
<br>
下载完成后，可进行完整性验证，支持校验和、公钥签名验证。
验证所需<a href="https://www.atzlinux.com/atzlinux-cd/10.3.3/amd64/iso-dvd/" target="_blank">相关文件请访问这里</a>获取。
<p>
<br>
该 iso 安装文件，支持目前国内市场上常用的 64 位 CPU。
除操作系统常用功能外，还
支持蓝牙网络热点接入，可以把手机 4G 网络通过蓝牙让电脑共享；
支持蓝牙音频源，可以把手机声音输出设置为电脑音箱。
支持 iPhone 手机通过数据线连接到电脑，拷贝手机上的照片到电脑。
支持部分型号摄像头，可用名为"茄子"的软件打开摄像头进行拍照和录视频。
</p>
<h2>安装</h2>
请务必在安装前<b>备份数据！</b>
笔记本电脑请连接电源线。
无线 WiFi 网络和有线网络支持自动获取网络信息。
<h3>支持安装方式</h3>
<ul>
	<li>物理机</li>
支持多系统共存；支持多个硬盘,可以在机器上任一硬盘上的任一分区上安装。
<h3>制作安装 U 盘</h3>
建议使用 U 盘制作 iso 文件安装盘进行安装：
在任一 Linux 系统下，可以使用如下命令制作安装 U 盘：（假设 U 盘为 /dev/sdb，U 盘的设备名可以在 U 盘插入后，用 root 用户在命令行执行
dmesg 命令，看最后输出的几行中，就有 sd 字符串开头的 U 盘设备名）
<br>
请用 root 账号执行：
<pre>

<i>dd if=atzlinux-10.3.3-amd64-DVD-1.iso of=/dev/sdb bs=4M status=progress</i>
</pre>
其它操作系统可以用各类光盘镜像制作软件制作。
如：UltraISO 软碟通（<a href="http://cn.ultraiso.net/xiazai.html" target="_blank">下载</a>
<a href="https://jingyan.baidu.com/article/19020a0a7dabaa529d28428b.html" target="_blank">
制作 u 盘启动盘教程</a>）、
<a href="https://www.onlinedown.net/soft/110173.htm" target="_blank">
Win32DiskImager</a>
<a name="biosefi" />
<h3>设置 BIOS EFI</h3>
关闭机器，插入 U 盘，重启机器，按键进入 BIOS 设置，在 BIOS 或 EFI 里面设置为 U 盘启动。该镜像同时支持老的 BIOS 主板和新的 EFI 主板。
<p>
注：
<ul>
不同机器开机进入 BIOS 设置的按键不一样
	<li>一般在开机屏幕时有提示。有按 Delete 键进入，也有按 F2 键……</li>
	<li>如果开机屏幕没有提示，建议查下主板说明书，或者上网查询、咨询机器厂家。</li>
</ul>
安装双系统的话，请主意要使用原有系统一样的启动模式来启动 U 盘安装。
<ul>
	<li>原有系统是使用 BIOS 启动</li>
	请继续在 BIOS 里面设置成 U 盘启动安装，安装过程中，不要使用 EFI 分区。
	<li>原有系统是使用 EFI 启动</li>
	请继续在 EFI 里面，设置成 U 盘启动安装，安装过程中，使用自动分区或者手工设置一个 EFI 分区。
</ul>
</p>
	<li>虚拟机</li>
支持 vmware、VirtualBox。内存设置为 2 G 即可正常运行。
</ul>
<h3>磁盘空间</h3>
建议 25G 以上。
<ul>
<li>新电脑</li>
建议使用整个硬盘空间。
<li>双系统</li>
建议先在原有系统上，把准备用来安装新系统的磁盘分区删除，使之成为<b>空闲磁盘空间</b>，然后再重启机器进行安装。
<li>虚拟机</li>
磁盘镜像文件建议 25 G 以上。
</ul>
<h3>安装过程</h3>
本 iso 文件的安装过程，已经进行了大量自动化定制，节省安装过程的手工输入。
安装过程中，需要人工输入的信息如下：
<ol>
	<li>如果是用 WiFi 连接网络，需要输入选择使用哪个 WiFi，并输入WiFi 密码。</li>
	如果是网线接入，这个步骤省略。
	<li>选择安装版本</li>
	根据不同用途，ISO 文件集成了 4 个软件包组合版本，供大家选择安装。
请按需选择其中的一个版本进行安装：
<pre>
 .
 debian-cn-all-txt-tech: 纯文本 命令行 技术人员版
 .
 debian-cn-amd64-xfce-mini: xfce 精简桌面环境 技术人员版
 .
 debian-cn-amd64-xfce: xfce 完整桌面环境 普通用户版
 .
 debian-cn-amd64-xall: xfce+gnome+mate 三合一桌面环境 普通用户版
</pre>
非技术人员，请选择“普通用户版”。
<a href="https://www.atzlinux.com/faq.htm#4ver" target="_blank">
4 个版本区别的详细说明
</a>
<p>
注：以上不同的软件包组合版本，只是安装的软件包搭配不同，不能够同时选择安装的。
如果想尽量多安装软件包，就请选择“三合一桌面环境 普通用户版”。
<br>
<b><font color="red">请只选择其中一个</font></b>安装，如果同时勾选多个，会导致<b>安装报错！</b>
</p>
<a name="demo" />
	<h4>《铜豌豆 Linux》三合一桌面环境 xfce 桌面<b>体验机器</b></h4>
在任何操作系统上，使用任一远程桌面程序（RDP 协议），可登录该体验环境。
<ul>
登录信息：
<li>主机：xall.atzlinux.com</li>
<li>端口：3389 为远程桌面 RDP 协议默认端口</li>
<li>用户名：wo wo1 wo2 三个中任一一个</li>
<li>密码： debian168;</li>
</ul>
<ul>
注意事项：
<li>桌面分辨率建议设置为 1024*768</li>
由于体验环境服务器带宽较小，如设置全屏或者大分辨率，可能操作响应会比较慢。
<li>请大家不要修改密码</li>
<li>每个用户，每次只能有一个人使用</li>
如遇使用过程中退出情况，可以更换用户名重新登录，或者稍等下
<li>请不要进行大流量传输</li>
<li>禁止任何违法行为！</li>
</ul>
	<li>对磁盘进行分区</li>
	根分区至少需要 10G 空间才可以完成安装。
	swap 分区一般按 1 倍内存，但不超过 2 G 为宜。
	建议 home 分区使用单独分区。
	<ul>
	<li>自动分区</li>
	安装程序可以自动识别磁盘的空闲空间，在操作磁盘分区的步骤中，选择空闲分区进行安装即可。推荐新手使用。
	<li>手工分区</li>
	手工分区，有搞错分区<b>丢失数据</b>的风险，请务必先在网络、移动硬盘或者其它机器备份数据！
请务必小心操作！在重新调整分区前，需要确认原有分区和 Linux 分区设备名的对应关系，不建议新手手工分区来安装多系统。
	<li>EFI 分区</li>
如果电脑是使用 EFI 启动，在安装时，需要有一个单独的 EFI 分区。EFI 分区只需要很小的磁盘空间，35 MB 就够了。
	</ul>
	<li>安装耗时</li>
	在分区操作完成后，接下来的安装过程，不再需要进行其它人为操作。
	三合一版本安装软件包数量最多，耗时最长。
	三合一版本，SSD 硬盘安装，预计 40 多分钟；机械硬盘，最长可达 2 小时。
	期间系统需要联网，自动下载少量更新文件，请确保网络畅通。
</ol>
<h3>用户登录</h3>
安装完成后，机器会自动重启进入新系统用户登录界面。
<ul>
	<li>用户登录时，三合一版本的用户，可以在右上角选择登录环境，支持 xfce、MATE、gnome 三种桌面环境。</li>
	<hr>
	系统默认创建两个用户，一个是具有最高系统权限的管理员用户 root ；一个是名字为 wo 的普通用户。
	<li>root 用户可以直接登录系统，默认密码为：</li>
	<i>debian-cn;168</i>
	<br>中间为英文的连接符，英文的分号
	<li>默认普通用户名为： <b>wo</b> ，密码为：</li>  <i>debian168;</i>
	<br>最后一个字符是英文的分号
	<hr>
	<li>请登录机器后，在终端命令行用 passwd 命令及时<b>修改密码，
<font color="red">
	root 用户和默认的普通用户 wo，都要修改密码！！！
</font></b>
<ul>
图形界面修改密码,不同桌面环境，修改密码的位置分别如下：
<li>xfce:点击桌面“口令”图标修改</li>
<li>MATE:顶部菜单栏“系统”--》“控制中心”--》“个人”--》“关于我”--》“更改密码”</li>
<li>gnome:点击顶部菜单右上角托盘图标区域，在弹出的菜单中点击设置图标（螺丝刀扳手）打开设置程序，再依次点击“用户”--》“密码”</li>
</ul>
	</li>
	<li>如果不需要使用默认的普通用户 wo，请用 root 创建新的用户后，再删除 wo 用户即可。
	<li>本系统没有安装任何网络远程登录服务端，<b>默认不开启网络远程登录</b>功能。</li>
如果需要允许远程登录，请切记在修改密码后，才安装远程登录相关服务。同时建议关闭远程登录的密码功能，使用带密码的私钥。
</ul>
<h3>网络</h3>
<ul>
	<li>使用动态 IP 方式连接网络</li>
	<li>主机名为：  atzlinux</li>
	<li>支持 ipv6</li>
	可以用 <i>nmtui</i> 命令修改网络连接方式、主机名。也可以点击网络图标调整。
	如果修改主机名，建议再手工修改下 /etc/hosts 文件，将 127.0.0.1 条目最后面，加上新修改的主机名。
</ul>
<h3>已知问题：</h3>
<ul>
<li>安装阶段</li>
<ol>
<li>部分笔记本电脑触控板在图形安装时，无法使用；在操作系统安装完成后能正常工作。请使用非图形安装，或者用键盘操作图形安装。</li>
<li>部分网卡在安装过程中无法使用，在操作系统安装完成后能正常工作。</li>
</ol>
<li>设备驱动</li>
少量 USB 无线网卡、高端显卡、HDMI 显卡集成声卡，默认没有驱动，需要在操作系统安装完成后再手工安装。
</ul>
在安装过程中，如出现无法识别网卡，或者无法连接网络的情况，可能需要在安装后手工
<a href="https://www.atzlinux.com/faq.htm#addapt" target="_blank">
添加 Debian 软件源</a>。
<h3>安装后优化</h3>
<ul>
	<li>安装 wine qq</li>
	<p>
因腾讯官方发布的 linuxqq 版本,目前功能还比较少，本系统还支持安装 wine qq。
<br>
这两个版本的 QQ 都可以在系统上使用。
请在命令行执行如下命令安装：
<p>
<i>
apt -y install deepin.com.qq.im
</i>
</p>
<a href="https://www.atzlinux.com/debian/download/wine-qq-install.log"  target="_blank">
安装过程</a>需要下载 200 多 M 文件，请耐心等待。安装完成后，请到应用程序菜单启动 QQ 即可使用，首次启动较慢。
</p>
<li>安装 wine 微信</li>
因部分微信账号被限制使用
<a href="https://web.wechat.com/" target="_blank" >网页版微信</a>，
本系统还支持安装 wine 微信。
<br>
这两个版本的微信都可以在系统上使用。
请在命令行执行如下命令安装：
<pre>

<i>apt -y install deepin.com.wechat</i>
</pre>
<li>安装其它软件</li>
请到铜豌豆软件源<a href="https://www.atzlinux.com/allpackages.htm">软件包列表</a>，按需安装其它软件。
</ul>
<h2>更新升级</h2>
<ul>
	<li>Debian 升级和安全更新，默认使用 Debian 国内官方镜像。</li>
	<li>本操作系统软件源仓库也设置在国内服务器上。</li>
</ul>
<p>
一键更新所有软件包到最新版本：
<br>
<br>
<i>
apt update
<br>
apt upgrade
</i>
</p>
<a href="https://www.atzlinux.com/debian/download/changelog.txt" target="_blank">
操作系统最新更新日志</a>，软件源新增加的软件包，也请在这里查看。
<h2>一键安装脚本</h2>
对于已经安装好 Debian 系统的用户，可以使用一键安装脚本一次性安装中文软件，
<a href="https://www.atzlinux.com/yjaz.htm">
详情请点击这里访问</a>。
<h2>开发</h2>
遵循 GPL3.0 协议,项目源代码地址：
<p>
<a href="https://gitee.com/atzlinux/debian-cn/tree/apt-install/" target="_blank">
https://gitee.com/atzlinux/debian-cn/tree/apt-install/
</a></p>
欢迎各位参与!
<h2>捐赠 《铜豌豆 Linux》项目</h2>
《铜豌豆 Linux》项目是开源项目，为维持项目持续发展运行，请积极
<a href="https://www.atzlinux.com/juanzeng.htm">
捐赠</a>。
建议通过 iso 安装操作系统的用户，<b>捐赠 10 元</b>以上。
<img src="./debian/img/wechat-pay.jpg" alt="微信收款码" width="44%">
<img src="./debian/img/ali-pay.jpg" alt="支付宝收款码" width="44%">
<p>
<a href="https://www.atzlinux.com/juanzeng.htm#liebiao">
捐赠列表</a></p>
<h2>联系方式</h2>
欢迎提供意见、建议，请用如下任一方式联系。
<ul>
	<li>邮箱：<a href="mailto:atzlinux@yeah.net" >atzlinux@yeah.net</a></li>
	<li>微信：atzlinux <a href="./debian/img/wechat.jpg" target="_blank" >点击链接扫码加微信</a></li>
	<li>微信群：Debian 中文桌面兴趣爱好小组，已经有 100 人，请加上面微信后，再拉入群。</li>
	<li>ＱＱ：<a href="https://user.qzone.qq.com/909868357/main" target="_blank" >909868357</a></li>
</ul>
<p>
欢迎：<a href="https://gitee.com/atzlinux/debian-cn/tree/apt-install/" target="_blank">发表点评</a>，<a href="https://gitee.com/atzlinux/debian-cn/issues" target="_blank">反馈问题，提交需求</a>。
</p>
<h2>安全性声明</h2>
Linux/Debian 的安全性在业界享有盛誉，本系统默认继承
<a href="https://www.debian.org/security/" target="_blank">
Debian 的所有安全机制</a>。
<ul>
<li>Debian 软件包</li>
已经设置好国内的 Debian 安全软件更新源，使用 <i>apt update;apt upgrade</i> 命令即可安装升级修复 Debian 软件包的所有安全补丁。
<li>商业软件</li>
对于使用的商业软件，都从商业公司官网下载，本系统上原则上不做修改；如商业软件的原有软件包有无法适配 Debian 的问题，也只做最小的适配修改，所有改动都开放源代码。
<li>开源软件</li>
对于采用的开源项目软件包，都从开源项目主页下载；有 deb 包，能够用的直接使用，需要适配 Debian 进行格式转换和修改的软件包，改动过程也全部开放代码。
<li>自制软件</li>
本系统自己制作的软件包，全部开放源代码。
</ul>
<a href="https://gitee.com/atzlinux/projects" target="_blank">
本项目所有源代码地址</a>
<p>
大家有发现本系统的任何安全问题，麻烦及时向<a href="mailto:atzlinux@yeah.net" >我反馈处理</a>，谢谢！
</p>
<h2>免责声明</h2>
<p>本操作系统基于开源软件和各个厂家的商业软件集成，对使用此操作系统造成的任何问题和损失，均不承担任何赔偿责任。<br>
相关商业软件的知识产权归各商业公司拥有，相关知识产权风险请自行承担。<br>
本项目基于 Debian ，但不属于 Debian 官方。本项目的任何问题，均与 Debian 官方项目无关。</p>
<p>
Debian 是 Software in the Public Interest, Inc. 的注册商标。
<br>
<a href="https://www.debian.org/trademark" target="_blank">Debian is a registered trademark owned by Software in the Public Interest, Inc.</a>
<br>本操作系统与 Debian 官方组织没有任何关系。
</p>
<p>
友情连接：
<a href="http://www.huzheng.org" target="_blank">星际译王</a>
<a href="http://shenzhenlug.org/" target="_blank">深圳 GNU/Linux 用户组</a>
<a href="http://szdiy.org/" target="_blank">深圳野生创客空间(SZDIY)</a>
<a href="https://blog.thinker.ink/" target="_blank">Suummmmer</a>
</p>
<p>
其它资源：
<a href="https://bss.csdn.net/m/topic/os2atc/index#a"  target="_blank">
OS2ATC 开源操作系统年度技术会议</a> 2019-12-14
<a href="http://39.107.37.69/debian/doc/os2atc2019/" target="_blank" >
资料下载</a>
</p>
<p><a href="http://bluefish.openoffice.nl" target="_blank">Made with Bluefish HTML editor.</a>
本文最后修改时间：2020-04-14
<a href="http://beian.miit.gov.cn" target="_blank">
粤ICP备19157078号-1</a>
<a href="http://www.beian.gov.cn/portal/registerSystemInfo?recordcode=44030502004897" target="_blank">
<img src="https://cdn.atzlinux.com/debian/img/ghs.png" alt="公安备案图标">
公安备案号 44030502004897</a>
</p>
<hr>
<a href="https://www.atzlinux.com/index.htm">首页</a>
<a href="https://www.atzlinux.com/yjaz.htm">一键安装脚本</a>
<a href="https://www.atzlinux.com/allpackages.htm">软件包列表</a>
<a href="https://www.atzlinux.com/skills.htm">使用技巧</a>
<a href="https://www.atzlinux.com/iso-type.htm">其它版本 ISO</a>
<a href="https://www.atzlinux.com/faq.htm">常见问题</a>
<a href="https://gitee.com/atzlinux/debian-cn/issues" target="_blank">反馈问题 bug</a>
<a href="https://www.atzlinux.com/devel.htm">参与开发</a>
<a href="https://gitee.com/atzlinux/projects" target="_blank">源代码</a>
<a href="https://www.atzlinux.com/juanzeng.htm">捐赠</a>
</div>
</body>
</html>
